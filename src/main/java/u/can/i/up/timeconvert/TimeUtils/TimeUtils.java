package u.can.i.up.timeconvert.TimeUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.FileTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {
	
	private final static TimeUtils mTimeUtils = new TimeUtils();
	private Date mDate;
	
	public final static TimeUtils getInstance() {
		return mTimeUtils;
	}
	public TimeUtils(){
		SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
		String dateString = "1970-01-01";
		try {
			mDate = ft.parse(dateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void changeTimeOfDirectory(String path) {
		File tmpFile = new File(path);
		if (tmpFile == null || !tmpFile.exists()) {
			return;
		}
		if (tmpFile.isDirectory()) {
			File[] fileLists = tmpFile.listFiles();
			for (int i = 0; i < fileLists.length; i++) {
				changeTimeOfDirectory(fileLists[i].getAbsolutePath());
//				tmpFile.setLastModified(mDate.getTime());
				setFileCreationDate(tmpFile.getAbsolutePath(), mDate);
			}
		}
		setFileCreationDate(tmpFile.getAbsolutePath(), mDate);
	}
	public void setFileCreationDate(String filePath, Date creationDate){
        BasicFileAttributeView attributes = Files.getFileAttributeView(Paths.get(filePath), BasicFileAttributeView.class);
        FileTime time = FileTime.fromMillis(creationDate.getTime());
        try {
			attributes.setTimes(time, time, time);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
